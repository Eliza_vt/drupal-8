<?php

namespace Drupal\ex_form\Form;

use Drupal\Core\Form\FormBase;                   // Базовый класс Form API
use Drupal\Core\Form\FormStateInterface;              // Класс отвечает за обработку данных

/**
 * Наследуемся от базового класса Form API
 * @see \Drupal\Core\Form\FormBase
 */


class ExForm extends FormBase {

    // метод, который отвечает за саму форму - кнопки, поля
    public function buildForm(array $form, FormStateInterface $form_state) {

        $form['title'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Ваше имя'),
            '#description' => $this->t('Имя не должно содержать цифр'),
            '#required' => TRUE,
        ];

        $form['E-mail'] = [
            '#type' => 'textfield',
            '#title' => $this->t('E-mail'),
            '#description' => $this->t('E-mail должен содержать @'),
            '#required' => TRUE,
        ];

        $form['Year'] = [
            '#type' => 'select',
            '#title' => $this->t('Год рождения'),
            '#required' => TRUE,
            '#options' => [
                '1' => $this
                    ->t('1995'),
                '2' => $this
                    ->t('1996'),
                '3' => $this
                    ->t('1997'),
                '4' => $this
                    ->t('1998'),
                '5' => $this
                    ->t('1999'),
                '6' => $this
                    ->t('2000'),
            ],
        ];

        $form['Sex'] = [
            '#type' => 'radios',
            '#title' => $this->t('Пол'),
            '#required' => TRUE,
            '#options' => array(
                0 => $this
                    ->t('Мужской'),
                1 => $this
                    ->t('Женский'),
            ),

        ];

        $form['Countkon'] = [
            '#type' => 'radios',
            '#title' => $this->t('Количество конечностей'),
            '#required' => TRUE,
            '#options' => array(
                0 => $this
                    ->t('3'),
                1 => $this
                    ->t('4'),
                2 => $this
                    ->t('5'),
            ),
        ];

        $form['Super'] = [
            '#type' => 'select',
            '#title' => $this->t('Суперспособности'),
            '#required' => TRUE,
            '#multiple' => TRUE,
            '#options' => [
                '1' => $this
                    ->t('Бессмертие'),
                '2' => $this
                    ->t('Прохождение сквозь стены'),
                '3' => $this
                    ->t('Левитация'),
            ],
        ];

        $form['Biography'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Биография'),
            '#required' => TRUE,
        ];

        $form['Checkbox'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('С контрактом ознакомлен'),
            '#required' => TRUE,
        ];

        // Add a submit button that handles the submission of the form.
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Отправить форму'),
        ];

        return $form;
    }

    // метод, который будет возвращать название формы
    public function getFormId() {
        return 'ex_form_exform_form';
    }

    // ф-я валидации
    public function validateForm(array &$form, FormStateInterface $form_state) {
        $title = $form_state->getValue('title');
        $is_number = preg_match("/[\d]+/", $title, $match);

        if ($is_number > 0) {
            $form_state->setErrorByName('title', $this->t('Строка содержит цифру.'));
        }
    }

    // действия по сабмиту
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $title = $form_state->getValue('title');
        drupal_set_message(t('%title, спасибо, ваша анкета отправлена', ['%title' => $title]));
        $module = 'ExForm';
        $key = 'test';
        $to = "elizavetatelyatnik@gmail.com";
        $langcode = "ru";
        $mailManager = \Drupal::service('plugin.manager.mail');
        $params = [];
        $params['message'] = "Форма заполнена";
        $send = true;
        $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
        if ($result['result'] !== true) {
            $message = t('There was a problem sending your email notification to @email.', array('@email' => $to));
            drupal_set_message($message, 'error');
            \Drupal::logger('ExForm')->error($message);
        } else {
            $message = t('An email notification has been sent to @email ', ['@email' => $to]);
            drupal_set_message($message, 'status');
            \Drupal::logger('ExForm')->notice($message);
        }
    }





}