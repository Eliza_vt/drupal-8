<?php

namespace Drupal\write_log\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class ExampleSettingsForm extends ConfigFormBase {

    /**
     * Config settings.
     *
     * @var string
     */
    const SETTINGS = 'write_log.settings';

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'example_admin_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            static::SETTINGS,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config(static::SETTINGS);
        echo $config->get('Flag');
        $form['example_thing'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Анонимные пользователи могут указывать контактную информацию'),
            '#default_value' => $config->get('Flag'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        // Retrieve the configuration.
        $this->configFactory->getEditable(static::SETTINGS)
            // Set the submitted configuration setting.
            ->set('Flag', $form_state->getValue('example_thing'))
            // You can set multiple configurations at once by making
            // multiple calls to set().
//      ->set('other_things', $form_state->getValue('other_things'))
            ->save();

        parent::submitForm($form, $form_state);
    }

}