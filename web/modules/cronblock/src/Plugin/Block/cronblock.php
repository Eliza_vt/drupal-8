<?php


namespace Drupal\cronblock\Plugin\Block;

use Drupal\Core\Logger\RfcLoggerTrait;
use Psr\Log\LoggerInterface;


/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "cron_block",
 *   admin_label = @Translation("Cronblock"),
 *   category = @Translation("cronblock"),
 * )
 */
class cronblock implements LoggerInterface {
    use RfcLoggerTrait;

    function cronblock_cron() {

        // Short-running operation example, not using a queue:
        // Delete all expired records since the last cron run.
        $expires = \Drupal::state()
            ->get('cronblock.last_check', 0);
        \Drupal::database()
            ->delete('cronblock_table')
            ->condition('expires', $expires, '>=')
            ->execute();
        \Drupal::state()
            ->set('cronblock.last_check', REQUEST_TIME);

        // Long-running operation example, leveraging a queue:
        // Queue news feeds for updates once their refresh interval has elapsed.
        $queue = \Drupal::queue('aggregator_feeds');
        $ids = \Drupal::entityTypeManager()
            ->getStorage('aggregator_feed')
            ->getFeedIdsToRefresh();
        foreach (Feed::loadMultiple($ids) as $feed) {
            if ($queue
                ->createItem($feed)) {

                // Add timestamp to avoid queueing item more than once.
                $feed
                    ->setQueuedTime(REQUEST_TIME);
                $feed
                    ->save();
            }
        }
        $ids = \Drupal::entityQuery('aggregator_feed')
            ->condition('queued', REQUEST_TIME - 3600 * 6, '<')
            ->execute();
        if ($ids) {
            $feeds = Feed::loadMultiple($ids);
            foreach ($feeds as $feed) {
                $feed
                    ->setQueuedTime(0);
                $feed
                    ->save();
            }
        }
    }

}