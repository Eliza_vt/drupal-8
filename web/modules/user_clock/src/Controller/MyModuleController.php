<?php

/**
 * @file
 * Contains \Drupal\hello_world\Controller\PageController.
 */

namespace Drupal\user_clock\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MyModuleController extends ControllerBase {

  /**
   * Page callback example.
   */

  public function content(Request $request, $user) {
    $account = \Drupal::currentUser();
    $myForm = $this->formBuilder()->getForm('Drupal\user_clock\Form\HelloForm');
    $renderer = \Drupal::service('renderer');
    $myFormHtml = $renderer->render($myForm);
    if ($account->id() == $user || $account->id() == 1) {
      return [
        '#markup' => $myFormHtml,
      ];
    }
    else
      throw new NotFoundHttpException();
  }

}