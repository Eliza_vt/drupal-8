<?php

namespace Drupal\deletenode\Form;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;


/**
 * Defines a simple form for deleting node.
 */
class DeleteNode extends FormBase implements BaseFormIdInterface {

  /**
   * Node ID of product this form is attached to.
   *
   * @var string
   */
  protected $nid;

  /**
   * Constructs a DeleteNode.
   *
   * @param string $nid
   *   The node ID.
   */
  public function __construct($nid) {
    $this->nid = $nid;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_form_' . $this->nid;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {

    $form['nid'] = [
      '#type' => 'value',
      '#value' => $node->id(),
    ];

    $form['submit'] = [
      "#type" => "submit",
      '#value' => $this->t('Удалить'),
    ];


    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    try {
      Node::load($form_state->getValue('nid'))->delete();
      $this->redirect('<front>');
      \Drupal::messenger()->addMessage('Нод удален успешно');
    } catch (EntityStorageException $e) {
      \Drupal::messenger()->addError('Что-то пошло не так');
    }

  }

}

