<?php

/**
 * @file
 * Contains \Drupal\hello_world\Controller\PageController.
 */

namespace Drupal\hello_world\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;

class PageController extends ControllerBase {

/**
 * Page callback example.
 */

public function page() {
    return [
      '#markup' => $this->t('Hello, World!'),
    ];
}

  public function uid(Request $request, $uid) {
    $account = \Drupal::currentUser();
      if ($account->id() == $uid || $account->id() == 1) {
        return [
          '#markup' => \Drupal::service('timemodule.hello_message')->getMessage(),
        ];
      }
    else
      return $this->redirect('hello_world.page');
  }

}